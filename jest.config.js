module.exports = {
    verbose: true,
    roots: [
        "<rootDir>/src"
    ],
    transform: {
        ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "jest-transform-stub",
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
    testMatch: [
        "**/?(*.)+(spec|test).ts?(x)"
    ]
}