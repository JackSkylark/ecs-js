﻿import BitSet from "fast-bitset";

export default function* bitmaskGenerator()
{
    let n = 1;

    while(true)
    {
        const mask = new BitSet(n);
        mask.set(n-1);
        ++n;

        yield mask;
    }
}
