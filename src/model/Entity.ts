import { Component } from "../component";
import { ComponentConstructor } from "./ComponentConstructor";
import { EntityId } from "./EntityId";

export interface Entity
{
    readonly id: EntityId;
    destroy: () => void;
    getComponent: <T extends Component>(ctor: ComponentConstructor) =>  T | undefined;
    addComponents: (...components: Component[]) => Entity;
    removeComponents: (...ctors: ComponentConstructor[]) => void;
}
