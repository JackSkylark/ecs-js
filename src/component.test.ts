import { Component } from "./component";

class Component1 extends Component {}
class Component2 extends Component {}

describe("Component", () => {
    test("component classes should have unique bitmasks", () => {
        expect(Component1.bitmask.isEqual(Component2.bitmask)).toBe(false)
    });
});
