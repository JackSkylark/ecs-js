import { Component } from "./component";
import { ComponentConstructor, Entity } from "./model";

export abstract class System
{
    public abstract query: ComponentConstructor[];
    public abstract run: (dt: number, entities: Entity[]) => void;
}

class TestComponent extends Component
{
    test: string = "";
}

class RenderingSystem extends System
{
    public run = (dt: number, entities: Entity[]) =>
    {
        for (const entity of entities) {
            const testComponent = entity.getComponent<TestComponent>(TestComponent);
        }
    };

    public query: ComponentConstructor[] = [];
}
