import { Component } from "./component";
import { ComponentMap } from "./componentMap";

class Component1 extends Component {}
class Component2 extends Component {}

describe("ComponentMap", () => {
    it("should update bitmap when adding components", () =>
    {
        const cm = new ComponentMap();
        cm.set(new Component1());

        expect(cm.bitmask.and(Component1.bitmask).isEqual(Component1.bitmask)).toBe(true);
        expect(cm.bitmask.and(Component2.bitmask).isEqual(Component2.bitmask)).toBe(false);
    
        cm.set(new Component2());

        expect(
            Component1.bitmask
            .or(Component2.bitmask)
            .and(cm.bitmask)
            .isEqual(cm.bitmask)
        ).toBe(true);
    });

    it("should update bitmap when removing components", () => {
        const cm = new ComponentMap();
        cm.set(new Component1());
        cm.set(new Component2());
        cm.remove(Component2);

        expect(cm.bitmask.and(Component1.bitmask).isEqual(Component1.bitmask)).toBe(true);
        expect(cm.bitmask.and(Component2.bitmask).isEqual(Component2.bitmask)).toBe(false);
    });

    it("should support clearing components", () => {
        const cm = new ComponentMap();
        cm.set(new Component1());

        cm.clear();

        expect(cm.bitmask.isEmpty()).toBe(true);
        expect(cm.get(Component1)).toBeUndefined();
    });
});
