import { Component } from "./component";
import { EntityStore } from "./entityStore";

class Component1 extends Component {};
class Component2 extends Component {};
class Component3 extends Component {};

describe("entityStore", () => 
{
    describe("createEntity()", () =>
    {
        it("returns a new entity", () =>
        {
            const entityStore = new EntityStore();
            expect(entityStore.createEntity().id).toBeDefined();
        });

        it("recycles entities", () => 
        {
            const entityStore = new EntityStore();
            const entity = entityStore.createEntity();
            
            entityStore.deleteEntity(entity.id);

            const entity2 = entityStore.createEntity();
            expect(entity.id).toBe(entity2.id);
        });  
    })
});

